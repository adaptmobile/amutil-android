package dk.adaptmobile.amutil;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Alex on 30/12/16.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AMDateUtilTest.class,
        CheckTest.class })

public class TestSuite {
}
